/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

PreviewerService réalise la conversion d'un document distant
en image de pré visualisation de ce document au format PNG.

La conversion est réalisée par une API REST qui retourne un fichier PNG
dans le corps de la réponse à un GET sur /preview,
avec l'URL du document à convertir passée en paramètre de la requête.

Le format de la page de pré visualisation est celle du document à convertir.
Ce format peut être  redéfini par des paramètres optionnels passés dans la requête:
    - orient: portrait ou landscape
    - format: de A4 jusqu'à A0

Dans tous les cas, une image PNG est fournie en réponse à la requête.

Exemple d'usage de l'application:
    - disposer d'un ordinateur hôte avec LibreOffice, une JVM, chromium-browser et chromium-chromedriver installés

    - exécuter le jar situé dans le dossier PreviewerService:
        java -jar PreviewerService-1.0.2-jar-with-dependencies.jar

    - faire un appel à l'API depuis un navigateur:
        . exemple de conversion d'un document HTML:
          http://localhost:55040/preview?url=http://www.orange.com

        . exemple de conversion d'un document
          http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls

        . conversion avec orientation paysage et format A3
          http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls&orient=landscape&format=A3

    - jeter un oeil aux traces dans le terminal où est exécuté le jar
