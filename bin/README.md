# PreviewerService

PreviewerService offers a simple REST API that gives back a PNG image in the response body of a GET /preview request on the API.
The PNG image represents the first page of the document to be previewed.

The URL of the document to be previewed is passed as parameter in the GET request.

The page format of the preview is the same as the document to be converted.
Optional parameters can be passed in the GET:

    - orient: portrait or landscape
    - format: A4 to A0

In any case, a PNG picture is sent back to the request.

## Build & Run

Build the jar:

    // go to PreviewerService Folder and run the following command:
    > mvn install
    
    // The jar built with dependencies is placed under PreviewerService/target/,
    // move it under PreviewerService/
    > mv target/PreviewerService-1.0.1-jar-with-dependencies.jar .    

Usage:

    // have a host with LibreOffice and a JVM
    
    // run the jar in the PreviewerService folder:
    > java -jar PreviewerService-1.0.1-jar-with-dependencies.jar

    // call the API with a browser:
    // html conversion example
    http://localhost:55040/preview?url=http://www.orange.com
    
    // document conversion example
    http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls
    
    // conversion example with orient set to landscape and format set to A3
    http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls&orient=landscape&format=A3
    
    // take a look on log traces in the console running the jar

## License

@Copyright 2020 Orange

This software is distributed under the terms of the MPL v2 license, please see license file.
Images under the `drawImages` & `noPreviewDir` directories are distributed under the terms of the CC BY 4.0 creative commons license, please see license file.

## Authors

Pierre Dekyndt
