/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

import org.apache.tika.Tika;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.orange.previewer.PreviewHandler.workingDir;
import static com.orange.previewer.PreviewHandler.workingFileName;

/**
 * Performs the guess of a remote file extension based on its url thanks to Apache Tika features.
 * Downloads this remote file if its extension is supported by conversion process
 */
class RemoteResourceManagement
{

    /** Tika url detection forbidden extension list
     */
    private static final List<String> invalidExtensionList = new ArrayList<>();
    static
    {
        invalidExtensionList.add("");
        invalidExtensionList.add(".bin");
        invalidExtensionList.add(".zip");
    }

    private static final int RESOURCE_MAX_SIZE = 20000000; // bytes

    private static final long GUESS_TIME_OUT_MS = 30000; // ms


    static String guessFileExtension (String fileUrl) throws PreviewerException
    {
        // get resource status code on connection
        String noWhiteSpaceURL = fileUrl.replace(" ", "%20");
        URL url;
        try {
            url = new URL(noWhiteSpaceURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new PreviewerException(PreviewerException.BAD_RES_URL);
        }
        HttpURLConnection urlConnection;
        int resourceGetStatusCode;
        String resourceGetStatusMessage;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            resourceGetStatusCode = urlConnection.getResponseCode();
            resourceGetStatusMessage = urlConnection.getResponseMessage();
        } catch (IOException e) {
            e.printStackTrace();
            throw new PreviewerException(PreviewerException.CON_RES_ERROR);
        }
        urlConnection.disconnect();

        if (resourceGetStatusCode >= 400) {
            // cannot connect to the resource, throw returned status code
            throw new PreviewerException(resourceGetStatusCode, resourceGetStatusMessage);
        }
        else {
            // use of service execution to limit callable execution time for a given period
            final ExecutorService executor = Executors.newSingleThreadExecutor();
            // define the callable function that will run under execution service control
            Callable<String> callable = () -> guessFileExtensionMaybeDownloadIt(fileUrl);
            // submit the callable function to the executor service
            Future<String> future = executor.submit(callable);
            String extension;
            try
            {
                // waits for end of execution
                extension = future.get(GUESS_TIME_OUT_MS, TimeUnit.MILLISECONDS);
            }
            catch (TimeoutException e)
            {
                // callable still running, stop execution and service
                future.cancel(true);
                executor.shutdownNow();
                throw new PreviewerException(PreviewerException.GUESS_TIME_OUT);
            }
            catch (InterruptedException | ExecutionException e)
            {
                if (e.getCause().getClass().equals(PreviewerException.class))
                {
                    throw new PreviewerException(PreviewerException.FILE_TOO_BIG);
                }
                else
                {
                    System.out.println("!!!! error in RemoteResourceManagement !!!!");
                    e.printStackTrace();
                    throw new PreviewerException(PreviewerException.RES_DL_ERROR);
                }
            }
            finally
            {
                // stop execution service
                executor.shutdown();
            }
            return extension;
        }
    }


    private static String guessFileExtensionMaybeDownloadIt(String fileUrl)
            throws IOException, MimeTypeException, PreviewerException, NoSuchAlgorithmException, KeyManagementException
    {
        // replace withe spaces in url
        String noWhiteSpaceURL = fileUrl.replace(" ", "%20");
        // get URL object
        URL url = new URL(noWhiteSpaceURL);
        // get response Header
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.connect();
        int resourceGetStatusCode = urlConnection.getResponseCode();
        System.out.println("GET on resource response: " + resourceGetStatusCode);
        Map<String, List<String>> headerFieldMap = urlConnection.getHeaderFields();
        System.out.println("resource headerField Map: " + headerFieldMap);
        urlConnection.disconnect();
        // check response status: redirection can occur
        String extension = "";
        switch (resourceGetStatusCode)
        {
            case 200:
                // resource available: guess its extension and download it
                extension = guessFileExtensionAndLoadFile(headerFieldMap, noWhiteSpaceURL);
                break;
            case 301:
            case 302:
            case 307:
                // re direction: use location field recursively if defined
                if (headerFieldMap.containsKey("Location"))
                {
                    System.out.println("redirection on Location: " + headerFieldMap.get("Location").get(0));
                    extension = guessFileExtensionMaybeDownloadIt(headerFieldMap.get("Location").get(0));
                }
                else if (headerFieldMap.containsKey("location"))
                {
                    System.out.println("redirection on location: " + headerFieldMap.get("location").get(0));
                    extension = guessFileExtensionMaybeDownloadIt(headerFieldMap.get("location").get(0));
                }
                break;
            default:
                // other cases: extension cannot be defined
                break;
        }
        return extension;
    }

    private static String guessFileExtensionAndLoadFile (Map<String, List<String>> headerFieldMap, String fileUrl)
            throws MimeTypeException, IOException, PreviewerException, NoSuchAlgorithmException, KeyManagementException
    {
        String fileExtension = "";
        // 1st guess extension: attempt with file name attachment
        if (headerFieldMap.containsKey("Content-Disposition")) {
            System.out.println("guess extension by content disposition");
            String contentDispo = headerFieldMap.get("Content-Disposition").get(0);
            Pattern pattern = Pattern.compile("(attachment; filename=\")\\w+[.](\\w+)[\"]");
            Matcher matcher = pattern.matcher(contentDispo);
            if (matcher.find())
            {
                fileExtension = "." + matcher.group(2);
                if (invalidExtensionList.contains(fileExtension))
                {
                    fileExtension = "";
                } else {
                    downloadResource(fileUrl, workingDir, workingFileName, fileExtension);
                }
            }
        }
        // 2nd guess extension: attempt with Tika
        if (fileExtension.equals(""))
        {
            System.out.println("guess extension by Tika with resource url");
            Tika tika = new Tika();
            MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
            // guess extension with file url
            String mimeType = tika.detect(fileUrl);
            MimeType guessType = allTypes.getRegisteredMimeType(mimeType);
            fileExtension = guessType.getExtension();
            if (invalidExtensionList.contains(fileExtension)) {
                // url detection not reliable, guess extension with file content by downloading it
                System.out.println("guess with resource url not reliable, try guess by Tika with downloaded file content");
                downloadResource(fileUrl, workingDir, workingFileName, "");
                File file = new File(workingDir + workingFileName);
                mimeType = tika.detect(file);
                guessType = allTypes.getRegisteredMimeType(mimeType);
                fileExtension = guessType.getExtension();
                // file already downloaded, rename it with extension detected
                file.renameTo(new File(workingDir + workingFileName + fileExtension));
            }
            else {
                // url detection reliable, download the file
                downloadResource(fileUrl, workingDir, workingFileName, fileExtension);
            }
        }
        return fileExtension;
    }

    private static void downloadResource (String resourceUrl, String workingDir, String fileName, String extension)
            throws IOException, PreviewerException, NoSuchAlgorithmException, KeyManagementException
    {
        // set an all trusted trust manager for https sources
        setAllTrustedTrustManager();
        // format download url
        URL url = new URL(resourceUrl);
        // get the size of the resource
        URLConnection urlConnection = url.openConnection();
        urlConnection.connect();
        int fileSize = urlConnection.getContentLength();
        System.out.println("downloading " + (fileSize <= 0 ? "": fileSize + " bytes"));
        if (fileSize > RESOURCE_MAX_SIZE)
        {
            //throw new Big Size Resource Exception();
            throw new PreviewerException(PreviewerException.FILE_TOO_BIG);
        }
        // open download stream
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        // downloading
        FileOutputStream fos = new FileOutputStream(workingDir + fileName + extension);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        // close streams
        rbc.close();
        fos.close();
    }

    private static void setAllTrustedTrustManager() throws NoSuchAlgorithmException, KeyManagementException {
        // create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]
                {
                        new X509TrustManager()
                        {
                            @Override
                            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) { }
                            @Override
                            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) { }
                            @Override
                            public X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                        }
                };
        // install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier()
        {
            @Override
            public boolean verify(String s, SSLSession sslSession)
            {
                return true;
            }
        };
        // install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
}
