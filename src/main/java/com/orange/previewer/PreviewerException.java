/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

/**
 * for throwing Previewer exceptions when an error requesting a feedback to the client occurs during the conversion process
 */
class PreviewerException extends Exception {

    static final int FILE_TOO_BIG    = -1;
    static final int HTML_TIME_OUT   = -2;
    static final int OFFICE_TIME_OUT = -3;
    static final int GUESS_TIME_OUT  = -4;
    static final int CONV_ERROR      = -5;
    static final int BAD_RES_URL     = -6;
    static final int CON_RES_ERROR   = -7;
    static final int RES_DL_ERROR    = -8;

    private final int exceptionCode;
    private final String exceptionMessage;

    public PreviewerException (int exceptionCode) {
        this.exceptionCode = exceptionCode;
        this.exceptionMessage = buildExceptionMessage();
    }

    public PreviewerException(int exceptionCode, String exceptionMessage) {
        this.exceptionCode = exceptionCode;
        this.exceptionMessage = exceptionMessage;
    }

    int getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    private String buildExceptionMessage() {
        switch (exceptionCode) {
            case FILE_TOO_BIG:
                return "file to preview is too large, cannot perform preview";
            case HTML_TIME_OUT:
                return "HTMl page conversion takes too much time, cannot perform preview";
            case OFFICE_TIME_OUT:
                return "document conversion takes too much time, cannot perform preview";
            case GUESS_TIME_OUT:
                return "file to preview extension guessing takes too much time, cannot perform preview";
            case CONV_ERROR:
                return "an error occurred during conversion process, cannot perform preview";
            case BAD_RES_URL:
                return "file to preview url is malformed, cannot perform preview";
            case CON_RES_ERROR:
                return "an error occurred during connection to the file to preview, cannot perform preview";
            case RES_DL_ERROR:
                return "an error occurred during download of file to preview, cannot perform preview";
            default:
                return "";
        }
    }

}
