/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.http.HttpHeaders;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class handling requests on /preview route
 */
class PreviewHandler implements HttpHandler {

    // paths to files
    static final String workingDir = "./workingDir/";
    static final String workingFileName = "preview";
    static final String renderExtension = ".png";

    // list of extensions that will be processed by Chromium browser
    private static final List<String> htmlTypeExtensionList = new ArrayList<>();
    static {
        htmlTypeExtensionList.add(".html");
        htmlTypeExtensionList.add(".xhtml");
        htmlTypeExtensionList.add(".htm");
    }

    // list of extensions that will be unconverted because directly readable by the browser
    // source: https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types
    private static final List<String> unconvertedExtensionList = new ArrayList<>();
    static {
        unconvertedExtensionList.add(".png");
        unconvertedExtensionList.add(".jpg");
        unconvertedExtensionList.add(".jpeg");
        unconvertedExtensionList.add(".gif");
        unconvertedExtensionList.add(".bmp");
        unconvertedExtensionList.add(".webp");
    }

    // stores query parameters as name-value pair
    private static Map<String, String> paramMap;

    // called when a http request is received on /preview route
    @Override
    public void handle (HttpExchange httpExchange) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        // check if converter is not already busy. If busy send back overload picture
        if (PreviewerService.isConversionAvailable()) {
            try {
                // mark service as busy and start conversion process
                PreviewerService.setConversionBusy();
                // get encoded query from http exchange
                String rawQuery = httpExchange.getRequestURI().getRawQuery();
                // set parameters map from query
                paramMap = extractUrlParamMap(rawQuery);
                // set resource url
                String resourceUrl = "";
                // decode url
                if (paramMap.containsKey("url")) {
                    resourceUrl = URLDecoder.decode(paramMap.get("url"), StandardCharsets.UTF_8.name());
                }
                Date date = new Date();
                System.out.println("########## request Preview on " + formatter.format(date) + " ##########");
                System.out.println("resource url: " + resourceUrl);
                // guess remote file extension and try to download it
                String extension = RemoteResourceManagement.guessFileExtension(resourceUrl);
                System.out.println("guessed extension: " + extension);
                // send relevant png file in response
                if (extension.equals("")) {
                    // no extension detected
                    String msg = "no guessed extension from file to preview, cannot perform preview";
                    httpExchangeSendJsonResponse(httpExchange, 500, msg);
                    System.out.println(msg);
                } else if (htmlTypeExtensionList.contains(extension)) {
                    // html document: call chromium for better web page rendering
                    HtmlConverter.convertFileToPngPicture(workingDir + workingFileName + renderExtension, resourceUrl);
                    httpExchangeSendJsonResponse(httpExchange, new File(workingDir + workingFileName + renderExtension));
                } else if (unconvertedExtensionList.contains(extension)) {
                    // image directly readable by the browser, no conversion needed
                    httpExchangeSendJsonResponse(httpExchange, new File(workingDir + workingFileName + extension));
                } else {
                    // Office document: call libre office
                    OfficeConverter.convertFileToPngPicture(workingDir, workingFileName, extension);
                    httpExchangeSendJsonResponse(httpExchange, new File(workingDir + workingFileName + renderExtension));
                }
            // if something went wrong during conversion process, send back error picture
            } catch (PreviewerException e) {
                switch (e.getExceptionCode()) {
                    case PreviewerException.FILE_TOO_BIG:
                    case PreviewerException.GUESS_TIME_OUT:
                    case PreviewerException.HTML_TIME_OUT:
                    case PreviewerException.OFFICE_TIME_OUT:
                    case PreviewerException.CONV_ERROR:
                    case PreviewerException.BAD_RES_URL:
                    case PreviewerException.CON_RES_ERROR:
                    case PreviewerException.RES_DL_ERROR:
                        httpExchangeSendJsonResponse(httpExchange, 500, e.getExceptionMessage());
                        System.out.println(e.getExceptionMessage());
                        break;
                    default:
                        String msg = "file to preview connection error: " + e.getExceptionMessage();
                        httpExchangeSendJsonResponse(httpExchange, e.getExceptionCode(), msg);
                        System.out.println(msg + ", status code = " + e.getExceptionCode());
                        break;
                }
            // thrown by URLDecoder.decode()
            } catch (UnsupportedEncodingException e) {
                String msg = "Unsupported URL Encoding Exception, no conversion performed";
                httpExchangeSendJsonResponse(httpExchange, 500, msg);
                System.out.println(msg);
                e.printStackTrace();
            } finally {
                // conversion is ended: set converter available again
                PreviewerService.setConversionAvailable();
                Date date = new Date();
                System.out.println("end of conversion on " + formatter.format(date) + "\n");
            }
        } else {
            // converter is busy
            String msg = "preview process is busy, cannot perform preview";
            httpExchangeSendJsonResponse(httpExchange, 500, msg);
            System.out.println(msg);
        }
    }

    private Map<String, String> extractUrlParamMap (String url) {
        Map<String, String> paramMap = new HashMap<>();
        // check that query starts with url=
        String urlKey = url.substring(0, 4);
        if (urlKey.equals("url=")) {
            // get string after url=
            String cut = url.substring(4);
            // split each parameter
            String[] params = cut.split("&");
            // first element from split is the resource url
            paramMap.put("url", params[0]);
            // store other parameters in the map
            if (params.length > 1) {
                for (int i = 1; i < params.length; i ++) {
                    String[] pair = params[i].split("=");
                    if (pair.length > 1) {
                        paramMap.put(pair[0], pair[1]);
                    } else {
                        paramMap.put(pair[0], "");
                    }
                }
            }
        }
        //paramMap.forEach((k, v) -> System.out.println(k + " : " + v));
        return paramMap;
    }

    private void httpExchangeSendJsonResponse (HttpExchange httpExchange, int statusCode, String message) {
        httpExchangeSendJsonResponse (httpExchange, statusCode, message, null);
    }

    private void httpExchangeSendJsonResponse (HttpExchange httpExchange, File file) {
        httpExchangeSendJsonResponse (httpExchange, 200, "", file);
    }

    private void httpExchangeSendJsonResponse (HttpExchange httpExchange, int statusCode, String message, File file) {
        // add the required response header
        Headers headers = httpExchange.getResponseHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        try {
            // send response and close the stream
            String response = buildJsonResponse(statusCode, message, file);
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(response.getBytes());
            outputStream.close();
        } catch (IOException e) {
            System.out.println("error in http exchange response");
            e.printStackTrace();
        } finally {
            // close the exchange
            httpExchange.close();
        }
    }

    private String buildJsonResponse (int statusCode, String message, File file) throws IOException {
        String encodedImage;
        if (file != null) {
            byte[] bytesBuffer = new byte[(int)file.length()];
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            bufferedInputStream.read(bytesBuffer, 0, bytesBuffer.length);
            bufferedInputStream.close();
            fileInputStream.close();
            encodedImage = new String(Base64.getEncoder().encode(bytesBuffer));
        }
        else {
            encodedImage = "";
        }
        return "{\"code\":" + statusCode + ", " +
                "\"msg\":\"" + message + "\"" + ", " +
                "\"img\":\"" + encodedImage +"\"}";
    }

    static Map<String, String> getParamMap () {
        return paramMap;
    }
}
