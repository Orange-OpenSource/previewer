/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


package com.orange.previewer;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * PreviewerService offers a simple REST API that gives back a JSON object
 * in the response body of a GET /preview request.
 * The JSON object is the following:
 * {
 *     code : 0,
 *     msg  : "",
 *     img  : ""
 * }
 * where code is the status of the preview operation:
 *     - 200: preview done
 *     - 500: error during the conversion process
 *     - >= 400: problem with connection to the file to preview.
 * msg is the error message given back by the preview process.
 * img is the preview of the file in PNG format, coded in Base64
 * The url of the document to be converted is passed as parameter in the GET request.
 * Optional parameters can be given in the GET:
 *      - orient: portrait or landscape
 *      - format: A4 to A0
 *
 */


public class PreviewerService {

    // version printed to console logs
    private static final String VERSION = "2.0.1";

    // http server port
    private static final int previewHttpPort = 55040;

    // api preview route
    private static final String PREVIEW = "/preview";

    // regex pattern for host and port extraction
    private static final Pattern URL_PATTERN = Pattern.compile("^(.*:)//([A-Za-z0-9\\-\\.]+)(:[0-9]+)?(.*)$");

    // proxy environment variables
    public static final String HTTP_PROXY = getEnvVar("http_proxy");
    public static final String HTTPS_PROXY = getEnvVar("https_proxy");
    public static final String NO_PROXY = getEnvVar("no_proxy")
                                            .replaceAll(",", "|")
                                            .replaceAll(" ", "");

    // set when a request is processed. Used to block any request when a conversion is on going
    private static boolean conversionIsBusy = false;

    public static void main(String[] args) {

        //System.getenv().forEach((k, v) -> System.out.println(k + " : " + v));
        //System.out.println("NO_PROXY = " + NO_PROXY);

        // JVM proxy configuration
        String httpHost = getHostFromUrl(HTTP_PROXY);
        String httpPort = getPortFromUrl(HTTP_PROXY);
        String httpsHost = getHostFromUrl(HTTPS_PROXY);
        String httpsPort = getPortFromUrl(HTTPS_PROXY);
        if (! httpHost.isEmpty() && ! httpPort.isEmpty()) {
            System.setProperty("http.proxyHost", httpHost);
            System.setProperty("http.proxyPort", httpPort);
        }
        if (! httpsHost.isEmpty() && ! httpsPort.isEmpty()) {
            System.setProperty("https.proxyHost", httpsHost);
            System.setProperty("https.proxyPort", httpsPort);
        }
        if (! NO_PROXY.isEmpty()) {
            System.setProperty("http.nonProxyHosts", NO_PROXY);
        }

        // getting webdriver logs
        //System.setProperty("webdriver.chrome.logfile", "chromedriver.log");
        //System.setProperty("webdriver.chrome.verboseLogging", "true");

        // set up executor for HttpServer: each request is handled immediately
        final ExecutorService executorService = Executors.newCachedThreadPool();
        try {
            // create server on the given port
            final HttpServer httpServer = HttpServer.create(new InetSocketAddress(previewHttpPort), 0);
            // create route and its callback
            httpServer.createContext(PREVIEW, new PreviewHandler());
            // create executor with multi threading capabilities
            httpServer.setExecutor(executorService);
            // start server
            httpServer.start();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            System.out.println("Previewer version " + VERSION + " started on " + formatter.format(date) + "\n");
            String sep = httpPort.isEmpty() ? "" : ":";
            System.out.println("http proxy setting  : " + httpHost + sep + httpPort);
            sep = httpsPort.isEmpty() ? "" : ":";
            System.out.println("https proxy setting : " + httpsHost + sep + httpsPort);
            System.out.println("no proxy setting    : " + NO_PROXY.replaceAll("\\|", " ") + "\n");
        } catch (IOException e) {
            System.out.println("!!!! error in PreviewerService !!!!");
            e.printStackTrace();
        }
    }

    // manage service access to conversion in a synchronised way
    static synchronized boolean isConversionAvailable() {
        if (!conversionIsBusy) {
            return true;
        }
        return false;
    }

    static synchronized void setConversionBusy () {
        conversionIsBusy = true;
    }

    static synchronized void setConversionAvailable () {
        conversionIsBusy = false;
    }

    private static String getEnvVar (String envKey) {
        return System.getenv().getOrDefault(envKey, "");
    }

    public static String getHostFromUrl(String url) {
        Matcher matcher = URL_PATTERN.matcher(url);
        if (matcher.matches() && matcher.groupCount() >= 2)
            return matcher.group(2);
        else
            return "";
    }

    public static String getPortFromUrl(String url) {
        Matcher matcher = URL_PATTERN.matcher(url);
        if (matcher.matches() && matcher.groupCount() >= 3)
            return matcher.group(3).replaceFirst(":", "");
        else
            return "";
    }

}
