/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.*;

/**
 * The class HtmlConverter converts any web page to png preview thanks to Chromium screenshot feature
 * and Selenium Chromium driving capabilities
 */
class HtmlConverter {

    private static final long HTML_TIME_OUT_MS = 30000; // ms

    static void convertFileToPngPicture (String renderPath, String url) throws PreviewerException
    {
        // instantiate an Executor service with single thread
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        // define a callable conversion function
        Callable<Void> callable = () -> convertFileToPngFormat(renderPath, url);
        // instantiate a Future that will execute the callable function
        Future<Void> future = executorService.submit(callable);
        try {
            // start asynchronous process that will throw a TimeoutException after HTML_TIME_OUT_MS
            future.get(HTML_TIME_OUT_MS, TimeUnit.MILLISECONDS);
        // future not resolved before time out: stop all processes immediately
        } catch (TimeoutException e) {
            // cancel the asynchronous process
            future.cancel(true);
            // request shutting down the execution service immediately
            executorService.shutdownNow();
            // kill chromium browser
            try {
                Runtime.getRuntime().exec("pkill chromium-browser");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throw new PreviewerException(PreviewerException.HTML_TIME_OUT);
        // something went wrong, kill chromium
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("!!!! error in HtmlConverter !!!!");
            e.printStackTrace();
            try {
                Runtime.getRuntime().exec("pkill chromium-browser");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throw new PreviewerException(PreviewerException.CONV_ERROR);
        // conversion is ended, closing ...
        } finally {
            // stop the execution service
            executorService.shutdown();
        }
    }

    private static Void convertFileToPngFormat (String renderPath, String url) throws IOException {
        System.out.println("converting html document ....");
        // chromium options settings
        ChromeOptions chromeOptions = new ChromeOptions();
        // set binary path
        chromeOptions.setBinary("/usr/bin/chromium-browser");
        // proxy settings
        String host = PreviewerService.getHostFromUrl(PreviewerService.HTTP_PROXY);
        String port = PreviewerService.getPortFromUrl(PreviewerService.HTTP_PROXY);
        if (! host.isEmpty() && ! port.isEmpty()) {
            chromeOptions.addArguments("--proxy-server=" + host + ":" + port);
        }
        if (! PreviewerService.NO_PROXY.isEmpty()) {
            chromeOptions.addArguments("--proxy-bypass-list=" + PreviewerService.NO_PROXY.replaceAll("\\|", ";"));
        }
        // will run without display
        chromeOptions.addArguments("--headless");
        // can be run as root
        chromeOptions.addArguments("--no-sandbox");
        // accept all IPs
        chromeOptions.addArguments("--whitelisted-ips");
        // no scrollbar in the screenshot
        chromeOptions.addArguments("--hide-scrollbars");
        // screenshot size
        chromeOptions.addArguments("--window-size=1920,1358");
        // Web driver instantiation with chromium options
        WebDriver webDriver = new ChromeDriver(chromeOptions);
        // navigation to the given url
        webDriver.navigate().to(url);
        // take screenshot from browser view
        File sourceFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        // save screenshot in file
        FileUtils.copyFile(sourceFile, new File(renderPath));
        // quit the web driver
        webDriver.quit();
        // for callable function
        return null;
    }

}
