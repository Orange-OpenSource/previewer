/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

import com.sun.star.awt.Size;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XNameContainer;
import com.sun.star.drawing.XDrawPage;
import com.sun.star.drawing.XDrawPagesSupplier;
import com.sun.star.drawing.XDrawView;
import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.lang.XComponent;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.style.XStyle;
import com.sun.star.style.XStyleFamiliesSupplier;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.view.PaperOrientation;
import com.sun.star.view.XPrintable;
import org.jodconverter.filter.Filter;
import org.jodconverter.filter.FilterChain;
import org.jodconverter.office.OfficeContext;
import org.jodconverter.office.utils.Lo;

import java.util.HashMap;
import java.util.Map;

/**
 * Offers a custom JODConverter filter that handles office document format and orientation properties
 */
public class OfficePageFormat implements Filter {

    // stores image resolution based on input document format properties, input for JODConverter storeProperties() chained method
    private HashMap<String, Object> outputResolutionMap;

    OfficePageFormat() {
        super();
    }

    // callback for filterChain() chained method
    @Override
    public void doFilter(OfficeContext officeContext, XComponent xComponent, FilterChain filterChain) throws Exception {
        // instantiate a PageProperty that will be filled by attempt on each office service (Writer, Calc, Draw)
        PageProperty pageProperty = new PageProperty();
        try {
            // will throw an unhandled exception if the document is not a text document
            pageProperty = tryTextPageProperty(xComponent);
        } catch (Exception e) {
            assert true;
        }
        try {
            // will throw an unhandled exception if the document is not a calc document
            pageProperty = tryCalcPageProperty(xComponent);
        } catch (Exception e) {
            assert true;
        }
        try {
            // will throw an unhandled exception if the document is not a draw/impress document
            pageProperty = tryDrawPageProperty(xComponent);
        } catch (Exception e) {
            assert true;
        }
        // set page property with eventual landscape, width and height parameters passed with the GET query
        setPropertiesWithUrlParams(pageProperty);
        // set map for properties that will be called by JODConverter storeProperties() method
        outputResolutionMap = getResolutionPropertiesMap(pageProperty.isLandscape());
        // Querying the XPrintable interface from the given component
        XPrintable xPrintable = Lo.qi(XPrintable.class, xComponent);
        // get paper format properties linked with input document orientation
        PropertyValue[] props = getPaperFormatProperties(pageProperty);
        // set the document printer with given properties
        xPrintable.setPrinter(props);
        // Call the next filter
        filterChain.doFilter(officeContext, xComponent);
    }

    private PageProperty tryTextPageProperty(XComponent xComponent) throws Exception {
        // Querying for the interface XTextDocument (text interface) from the XComponent
        final XTextDocument xTextDocument = Lo.qi(XTextDocument.class, xComponent);
        // Create a cursor supplier on the text document controller
        final XTextViewCursorSupplier xTextViewCursorSupplier =
                Lo.qi(XTextViewCursorSupplier.class, xTextDocument.getCurrentController());
        // Get a cursor from the supplier
        final XTextViewCursor xTextViewCursor = xTextViewCursorSupplier.getViewCursor();
        // Get the property set of the cell's TextCursor
        final XPropertySet xPropertySet = Lo.qi(XPropertySet.class, xTextViewCursor);
        // Get the Page Style name at the cursor position
        final String pageStyleName = xPropertySet.getPropertyValue("PageStyleName").toString();
        // Get the StyleFamiliesSupplier interface of the document
        final XStyleFamiliesSupplier xSupplier = Lo.qi(XStyleFamiliesSupplier.class, xTextDocument);
        // Use the StyleFamiliesSupplier interface to get the XNameAccess interface of the actual style families
        final XNameAccess xFamilies = Lo.qi(XNameAccess.class, xSupplier.getStyleFamilies());
        // Access the 'PageStyles' Family
        final XNameContainer xFamily = Lo.qi(XNameContainer.class, xFamilies.getByName("PageStyles"));
        // Get the style of the current page from the PageStyles family
        final XStyle xStyle = Lo.qi(XStyle.class, xFamily.getByName(pageStyleName));
        // Get the property set of the style
        final XPropertySet xStyleProps = Lo.qi(XPropertySet.class, xStyle);
        // get document size
        Size size = (Size) xStyleProps.getPropertyValue("Size");
        // get document orientation
        boolean isLandscape = (boolean) xStyleProps.getPropertyValue("IsLandscape");
        System.out.println("TEXT - orientation " +
                (isLandscape ? "landscape" : "portrait") +
                " - width " + size.Width +
                " - height " + size.Height);
        return new PageProperty(isLandscape, size);
    }

    private PageProperty tryCalcPageProperty(XComponent xComponent) throws Exception {
        // querying for the interface XSpreadsheetDocument on the XComponent
        final XSpreadsheetDocument xSpreadsheetDocument = Lo.qi(XSpreadsheetDocument.class, xComponent);
        // Select the first spreadsheet, this is the one that will be exported by JODConverter
        XIndexAccess xIndexAccess = Lo.qi(XIndexAccess.class, xSpreadsheetDocument.getSheets());
        final XSpreadsheet xSpreadsheet = Lo.qi(XSpreadsheet.class, xIndexAccess.getByIndex(0));
        // Querying the XPropertySet interface on the selected spreadsheet
        final XPropertySet xPropertySet = Lo.qi(XPropertySet.class, xSpreadsheet);
        // Get the page style name of the selected spreadsheet
        final String pageStyleName = xPropertySet.getPropertyValue("PageStyle").toString();
        // Querying the XStyleFamiliesSupplier interface on the spreadsheet document
        final XStyleFamiliesSupplier xStyleFamiliesSupplier = Lo.qi(XStyleFamiliesSupplier.class, xSpreadsheetDocument);
        // Use the StyleFamiliesSupplier interface to get the XNameAccess interface of the actual style families
        final XNameAccess xNameAccess = xStyleFamiliesSupplier.getStyleFamilies();
        // Access the 'PageStyles' Family
        final XNameContainer xNameContainer = Lo.qi(XNameContainer.class, xNameAccess.getByName("PageStyles"));
        // Get the style of the current page from the PageStyles family
        final XStyle xStyle = Lo.qi(XStyle.class, xNameContainer.getByName(pageStyleName));
        // Get the property set of the style
        final XPropertySet xStyleProps = Lo.qi(XPropertySet.class, xStyle);
        // get document size
        Size size = (Size) xStyleProps.getPropertyValue("Size");
        // Get the page orientation from the style
        boolean isLandscape = (boolean) xStyleProps.getPropertyValue("IsLandscape");
        System.out.println("CALC - orientation " +
                (isLandscape ? "landscape" : "portrait") +
                " - width " + size.Width +
                " - height " + size.Height);
        return new PageProperty(isLandscape, size);
    }

    private PageProperty tryDrawPageProperty(XComponent xComponent) throws Exception {
        // Querying the XDrawPagesSupplier interface from the given document
        final XDrawPagesSupplier xDrawPagesSupplier = Lo.qi(XDrawPagesSupplier.class, xComponent);
        // Querying the XModel interface from the supplier
        final XModel xModel = Lo.qi(XModel.class, xDrawPagesSupplier);
        // get the current controlleer from the ùodel
        final XController xController = xModel.getCurrentController();
        // Querying the XDrawView interface from the controller
        final XDrawView xDrawView = Lo.qi(XDrawView.class, xController);
        // get the current page from the view
        final XDrawPage xDrawPage = xDrawView.getCurrentPage();
        // Querying the XPropertySet interface from the current page
        final XPropertySet xPropertySet = Lo.qi(XPropertySet.class, xDrawPage);
        // get the page orientation property
        final Object object = xPropertySet.getPropertyValue("Orientation");
        boolean isLandscape = true;
        if (object == PaperOrientation.PORTRAIT) {
            isLandscape = false;
        }
        // get document size
        Size size = new Size();
        size.Width = (int) xPropertySet.getPropertyValue("Width");
        size.Height = (int) xPropertySet.getPropertyValue("Height");
        System.out.println("DRAW - orientation " +
                (isLandscape ? "landscape" : "portrait") +
                " - width " + size.Width +
                " - height " + size.Height);
        return new PageProperty(isLandscape, size);
    }

    private void setPropertiesWithUrlParams (PageProperty pageProperty) {
        // get map of parameters retrieved from the url
        Map<String, String> paramMap = PreviewHandler.getParamMap();
        // paper format
        if (paramMap.containsKey("format")) {
            String format = paramMap.get("format");
            System.out.println(format + " format requested");
            Size s = getPaperSize(format, pageProperty.isLandscape());
            pageProperty.setSize(s);
        }
        // paper orientation
        if (paramMap.containsKey("orient")) {
            String orient = paramMap.get("orient");
            if (orient.equals("landscape")) {
                System.out.println("landscape orientation requested");
                // invert width and height if document orientation is not landscape
                if (!pageProperty.isLandscape()) {
                    Size s = new Size();
                    s.Height = pageProperty.getSize().Width;
                    s.Width = pageProperty.getSize().Height;
                    pageProperty.setSize(s);
                }
                pageProperty.setIsLandscape(true);
            } else if (orient.equals("portrait")) {
                System.out.println("portrait orientation requested");
                // invert width and height if document orientation is landscape
                if (pageProperty.isLandscape()) {
                    Size s = new Size();
                    s.Height = pageProperty.getSize().Width;
                    s.Width = pageProperty.getSize().Height;
                    pageProperty.setSize(s);
                }
                pageProperty.setIsLandscape(false);
            }
        }
    }

    private PropertyValue[] getPaperFormatProperties (PageProperty pageProperty) {
        PropertyValue[] properties = new PropertyValue[2];
        properties[0] = new PropertyValue();
        properties[0].Name = "PaperOrientation";
        if (pageProperty.isLandscape()) {
            properties[0].Value = PaperOrientation.LANDSCAPE;
        } else {
            properties[0].Value = PaperOrientation.PORTRAIT;
        }
        properties[1] = new PropertyValue();
        properties[1].Name = "PaperSize";
        properties[1].Value = pageProperty.getSize();
        return properties;
    }

    private HashMap<String, Object> getResolutionPropertiesMap (boolean isLandscape) {
        HashMap<String, Object> resProps = new HashMap<>();
        PropertyValue[] props = new PropertyValue[2];
        props[0] = new PropertyValue();
        props[1] = new PropertyValue();
        props[0].Name = "PixelWidth";
        props[1].Name = "PixelHeight";
        int smallSize = 1358;
        int largeSize = 1920;
        if (isLandscape) {
            props[0].Value = largeSize;
            props[1].Value = smallSize;
        } else {
            props[0].Value = smallSize;
            props[1].Value = largeSize;
        }
        resProps.put("FilterData", props);
        return resProps;
    }

    private Size getPaperSize (String paperFormat, boolean isLandscape) {
        int hVal = 42000;
        int wVal = 29700;
        switch (paperFormat) {
            case "A4":
                hVal = 29700;
                wVal = 21000;
                break;
            case "A3":
                hVal = 42000;
                wVal = 29700;
                break;
            case "A2":
                hVal = 59400;
                wVal = 42000;
                break;
            case "A1":
                hVal = 84100;
                wVal = 59400;
                break;
            case "A0":
                hVal = 118900;
                wVal = 84100;
                break;
            default:
                break;
        }
        Size size = new Size();
        if (isLandscape) {
            size.Height = wVal;
            size.Width = hVal;
        } else {
            size.Height = hVal;
            size.Width = wVal;
        }
        return size;
    }

    HashMap<String, Object> getResolutionMap () {
        return  outputResolutionMap;
    }

    // internal class representing an office page property regarding its orientation and size
    private static class PageProperty {
        boolean b;
        Size s;

        PageProperty () { // default: A3 landscape
            b = true;
            s = new Size();
            s.Height = 29700;
            s.Width = 42000;
        }

        PageProperty (boolean b, Size s) {
            this.b = b;
            this.s = s;
        }

        boolean isLandscape() {
            return b;
        }

        Size getSize () {
            return s;
        }

        void setIsLandscape(boolean b) {
            this.b = b;
        }

        void setSize (Size s) {
            this.s = s;
        }
    }

}
