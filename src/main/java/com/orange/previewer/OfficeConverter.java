/*
 * Copyright (c) 2021 Orange
 *
 * Authors: Pierre Dekyndt
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.orange.previewer;

import org.jodconverter.LocalConverter;
import org.jodconverter.office.LocalOfficeManager;
import org.jodconverter.office.OfficeException;
import org.jodconverter.office.OfficeUtils;

import java.io.File;
import java.util.concurrent.TimeoutException;

/** The class OfficeConverter allows to convert an office document
 * to a PNG image that represents the first page of the document.
 * The conversion itself is managed by LibreOffice API, which is handled by JODConverter framework.
 * The page orientation and size properties are taken by default from the input document,
 * they are overwritten by parameters orient or format of the query.
 * Page orientation and format of input document are defined by filterChain() function.
 * Output image resolution is defined by storeProperties() function.
 */
class OfficeConverter {

    private static final long OFFICE_TIME_OUT_MS = 180000; // ms

    static void convertFileToPngPicture(String workingDir, String fileName, String extension) throws PreviewerException {
        System.out.println("converting office document ....");
        // input and output files
        File inputFile = new File(workingDir + fileName + extension);
        File outputFile = new File(workingDir + PreviewHandler.workingFileName + PreviewHandler.renderExtension);
        // Create an office manager using the default configuration. The default port is 2002.
        final LocalOfficeManager officeManager = LocalOfficeManager
                .builder()
                .taskExecutionTimeout(OFFICE_TIME_OUT_MS)
                .install()
                .build();
        // conversion process
        try {
            // Start an office process and connect to the started instance (on port 2002).
            officeManager.start();
            // Instanciate a custom Filter object implementation that will define page orientation and size
            OfficePageFormat officePageFormat = new OfficePageFormat();
            // execute conversion
            LocalConverter
                    .builder()
                    .filterChain(officePageFormat)
                    .storeProperties(officePageFormat.getResolutionMap())
                    .build()
                    .convert(inputFile)
                    .to(outputFile)
                    .execute();
        } catch (OfficeException e) {
            if (e.getCause().getClass().equals(TimeoutException.class)) {
                throw new PreviewerException(PreviewerException.OFFICE_TIME_OUT);
            } else {
                System.out.println("!!!! error in OfficeConverter !!!!");
                e.printStackTrace();
                throw new PreviewerException(PreviewerException.CONV_ERROR);
            }
        } finally {
            // Stop the office process
            OfficeUtils.stopQuietly(officeManager);
        }
    }
}

