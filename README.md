# Previewer

previewer offers a simple REST API that gives back a JSON object in the response body of a GET /preview request on the API.

The JSON object given back is the following:

    {
        code : 0,
        msg  : "",
        img  : ""
    }

where code is the status of the preview operation:

    - 200: preview done
    - 500: error during the conversion process
    - >= 400: problem with connection to the file to preview.

msg is the error message given back by the preview process.

img is the preview of the file in PNG format, coded in Base64

The PNG image represents the first page of the document to be previewed.

The URL of the document to be previewed is passed as parameter in the GET request.

The page format of the preview is the same as the document to be converted.
Optional parameters can be passed in the GET:

    - orient: portrait or landscape
    - format: A4 to A0

## Build & Run

Build the jar:

    // go to previewer Folder and run the following command:
    > mvn install
    
    // The jar built with dependencies is placed under previewer/target/,
    // move it under previewer/
    > mv target/PreviewerService-2.0.1-jar-with-dependencies.jar .

Usage:

    // have a host with LibreOffice, a JVM, chromium-browser and chromium-chromedriver installed
    
    // run the jar in the previewer folder:
    > java -jar PreviewerService-2.0.1-jar-with-dependencies.jar

    // call the API with a browser:
    // html conversion example
    http://localhost:55040/preview?url=http://www.orange.com
    
    // document conversion example
    http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls
    
    // conversion example with orient set to landscape and format set to A3
    http://localhost:55040/preview?url=https://www.data.gouv.fr/s/resources/compte-administratif-2014-depenses-du-budget-principal/20160302-102806/CA_2013_Depenses_Budget_Principal.xls&orient=landscape&format=A3

Take a look on log traces in the console running the jar.

Preview result can be seen in a browser with the img field by passing its value in the following HTML bracket:

    <img src="data:image/png;base64, put here the img field value..." />

## License

@Copyright 2021 Orange

This software is distributed under the terms of the MPL v2 license, please see license file.

## Authors

Pierre Dekyndt
